#ifndef DIRECTIONLIGHTRENDER_H
#define DIRECTIONLIGHTRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>
#define PI 3.14159265f
class DirectionLightRender
{
public:
    DirectionLightRender() = default;
    void initsize(float r);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &camera,QVector3D &lightDirection);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    float r_;
    QVector<GLfloat> points_;
};

#endif // DIRECTIONLIGHTRENDER_H
