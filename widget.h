#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "directionlightrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void resizeGL(int w, int h) override;
    void initializeGL() override;
    void paintGL() override;
    void wheelEvent(QWheelEvent *event) override;

private:
    QVector3D camera_,center_,lightDirection_;
    QMatrix4x4 pMatrix;
    DirectionLightRender render_;
    int yRotation = 0;
};

#endif // WIDGET_H
